# cse-551-grad-project

## Usage

```bash
git config core.autoCrlf input
yarn install
# Make changes
git add . -p
git commit
```

Adding a dependency:

```bash
cd web/ # Or api/, or alexa/

yarn add package-name
```
